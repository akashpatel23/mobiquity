import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../../weather.service';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {

  city: string = '';
  ForecastData :{Temperature: number; SeaLevel:number; Date: string}[] = [];
  constructor(private route: ActivatedRoute, private weatherService: WeatherService) { 
    
  }

  ngOnInit(): void {
    let filteredData;
    this.city = this.route.snapshot.paramMap.get('city') as string;
    this.weatherService.GetCityForecastData(this.city).subscribe(data => {
      filteredData = data.list.filter(time => time.dt_txt.includes('09:00:00'));
      filteredData.forEach(timeStamp => {
        this.ForecastData.push({
          Temperature: timeStamp.main.temp,
          SeaLevel: timeStamp.main.sea_level,
          Date: timeStamp.dt_txt
        })
      })
    });
  }
}
