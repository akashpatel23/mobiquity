import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { ICityData, ICityWeatherData } from '../../weather.model';
import { WeatherService } from '../../weather.service';
import * as moment from 'moment';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  title = 'weatherApp';
  cities = ['London', 'Berlin', 'Paris', 'Prague', 'Madrid'];
  CitiesData: ICityData[] = [];
  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
    let requests: any[] = [];
    this.cities.forEach(city => {
      requests.push(this.weatherService.GetCityWeatherData(city));
    });
    forkJoin(requests).subscribe(data => {
      this.CitiesData = data.map(city => this.cityDataToViewModel(city as ICityWeatherData));
    });
  };

  protected cityDataToViewModel(city: ICityWeatherData) {
    const utcShiftInHours = city.timezone / 3600;
    return {
      Name: city.name,
      Temperature: city.main.temp,
      Sunrise: moment.unix(city.sys.sunrise).utc().add(utcShiftInHours, 'hours').format("hh:mm a"),
      Sunset: moment.unix(city.sys.sunset).utc().add(utcShiftInHours, 'hours').format("hh:mm a"),
    }
  };
}
