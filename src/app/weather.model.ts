export interface ICityWeatherData {
    coord: {
        lon: number,
        lat: number
    };
    weather: IWeatherModel[];
    base: string;
    main: {
        temp: number,
        feels_like: number,
        temp_min: number,
        temp_max: number,
        pressure: number,
        humidity: number
    };
    visibility: number;
    wind: IWindModel;
    clouds: {
        all: number;
    };
    dt: number;
    sys: ISystemModel;
    timezone: number;
    id: number;
    name: string;
    cod: number;
}

export interface ICityData {
    Name: string;
    Temperature: number;
    Sunrise: string;
    Sunset: string;
}

export interface ICityForecastModel {
    cod: string;
    message: number;
    cnt: number;
    list: ITimeStampForecast[];
    city: {
        id: number;
        name: string;
        coord: {
            lat: number;
            lon: number;
        },
        country: string;
        population: number;
        timezone: number;
        sunrise: number;
        sunset: number;
    };

}

interface IWeatherModel {
    id: number;
    main: string;
    description: string;
    icon: string;
}

interface IWindModel {
    speed: number;
    deg: number;
}

interface ISystemModel {
    type: number;
    id: number;
    country: string;
    sunrise: number;
    sunset: number;
}

interface ITimeStampForecast {
    dt: number,
    main: {
      temp: number,
      feels_like: number,
      temp_min: number,
      temp_max: number,
      pressure: number,
      sea_level: number,
      grnd_level: number,
      humidity: number,
      temp_kf: number
    },
    weather: [
      {
        id: number,
        main: string,
        description: string,
        icon: string
      }
    ],
    clouds: {
      all: number
    },
    wind: {
      speed: number,
      deg: number,
      gust: number
    },
    visibility: number,
    pop: number,
    sys: {
      pod: string
    },
    dt_txt: string
}