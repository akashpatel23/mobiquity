import { TestBed } from '@angular/core/testing';

import { WeatherService } from './weather.service';

describe('WeatherService', () => {
  let service: WeatherService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WeatherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get city weather data', () => {
    const result = service.GetCityWeatherData('https://api.openweathermap.org/data/2.5/weather?q=london&units=metric&appid=3d8b309701a13f65b660fa2c64cdc517');
    result.subscribe(data => {
      expect(data).toEqual({
        coord: {
          lon: -0.1257,
          lat: 51.5085
        },
        weather: [
          {
            id: 803,
            main: 'Clouds',
            description: 'broken clouds',
            icon: '04d'
          }
        ],
        base: 'stations',
        main: {
          temp: 21.55,
          feels_like: 21.09,
          temp_min: 19.49,
          temp_max: 23.43,
          pressure: 1011,
          humidity: 51
        },
        visibility: 10000,
        wind: {
          speed: 7.72,
          deg: 250
        },
        clouds: {
          all: 55
        },
        dt: 1627567883,
        sys: {
          type: 2,
          id: 2019646,
          country: 'GB',
          sunrise: 1627532389,
          sunset: 1627588434
        },
        timezone: 3600,
        id: 2643743,
        name: 'London',
        cod: 200
      });
    });
  });
});
