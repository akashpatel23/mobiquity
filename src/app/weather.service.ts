import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ICityForecastModel, ICityWeatherData } from './weather.model';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private appId = '3d8b309701a13f65b660fa2c64cdc517';
  private forecastCache: { [key: string]: Observable<ICityForecastModel> } = {};
  private weatherCache: { [key: string]: Observable<ICityWeatherData> } = {};
  constructor(private httpClient: HttpClient) { }

  GetCityWeatherData(city: string): Observable<ICityWeatherData> {
    if (!this.weatherCache[city]){
      const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${this.appId}`;
      this.weatherCache[city] = this.httpClient.get<ICityWeatherData>(url).pipe(
        map(response => response),
        shareReplay()
      );
    }
    return this.weatherCache[city];
  }

  GetCityForecastData(city: string): Observable<ICityForecastModel> {
    if (!this.forecastCache[city]) {
      const url = `https://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&appid=${this.appId}`;
      this.forecastCache[city] = this.httpClient.get<ICityForecastModel>(url).pipe(
        map(response => response),
        shareReplay()
      );
    }
    return this.forecastCache[city];
  }
}
